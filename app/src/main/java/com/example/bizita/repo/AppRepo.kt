package com.example.bizita.repo

import androidx.lifecycle.MutableLiveData
import com.example.bizita.model.UserDataResponseModel
import com.example.bizita.service.DataSource
import com.example.bizita.service.NetworkCall
import com.example.bizita.service.Resource

class AppRepo(private val dataSource: DataSource) {

    private val searchServiceCall = NetworkCall<UserDataResponseModel>()

    suspend fun getDataList():
            MutableLiveData<Resource<UserDataResponseModel>> =
        searchServiceCall.makeCall(dataSource.getDataList())
}