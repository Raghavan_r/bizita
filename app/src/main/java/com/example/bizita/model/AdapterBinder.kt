package com.example.bizita.model

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData

class AdapterBinder {

    private var successData = MutableLiveData<Succes>()

    val userName: LiveData<String> = MediatorLiveData<String>().apply {
        addSource(successData) {
            value = it.name
        }
    }
    val description: LiveData<String> = MediatorLiveData<String>().apply {
        addSource(successData) {
            value = it.description
        }
    }

    fun contactList(succes: Succes) {
        successData.value = succes
    }

}