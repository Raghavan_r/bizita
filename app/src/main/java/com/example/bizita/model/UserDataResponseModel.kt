package com.example.bizita.model
import com.google.gson.annotations.SerializedName


data class UserDataResponseModel(
    @SerializedName("location")
    val location: List<Location>?,
    @SerializedName("Success")
    val success: List<Succes>?
)

data class Location(
    @SerializedName("lat")
    val lat: String,
    @SerializedName("longg")
    val longg: String
)

data class Succes(
    @SerializedName("address")
    val address: String?,
    @SerializedName("category")
    val category: String?,
    @SerializedName("categoryid")
    val categoryid: String?,
    @SerializedName("contact")
    val contact: String?,
    @SerializedName("description")
    val description: String?,
    @SerializedName("empcode")
    val empcode: String?,
    @SerializedName("id")
    val id: String?,
    @SerializedName("image")
    val image: String?,
    @SerializedName("name")
    val name: String?
)