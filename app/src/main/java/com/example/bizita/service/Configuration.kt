package com.example.bizita.service

import org.koin.core.qualifier.named
import org.koin.dsl.module
private const val BASE_URL = "http://aryu.co.in"
private const val TIME_OUT = 30L


val configModule = module {
    single(named(name = "BASE_URL")) { BASE_URL }
    single(named(name = "TIME_OUT")) { TIME_OUT }
}