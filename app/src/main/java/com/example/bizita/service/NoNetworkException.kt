package com.example.bizita.service

import java.io.IOException

open class NoNetworkException : IOException() {

    override fun getLocalizedMessage(): String? = "No Internet Connection"
}