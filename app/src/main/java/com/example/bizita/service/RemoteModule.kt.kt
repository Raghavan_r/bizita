package com.example.bizita.service

import android.content.Context
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.core.qualifier.named
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

/**
 * Remote Web Service datasource
 */
val remoteDataSourceModule = module {

    factory {
        createOkHttpClient(get(), get(named("TIME_OUT")))
    }

    factory {
        createWebService<DataSource>(get(), get(named("BASE_URL")))
    }

}

/**
 * This method used to create okHttp client
 * */
fun createOkHttpClient(context: Context, timeOut: Long): OkHttpClient {

    val clientBuilder =
        OkHttpClient.Builder()
            .connectTimeout(timeOut, TimeUnit.SECONDS)
            .readTimeout(timeOut, TimeUnit.SECONDS)
            .writeTimeout(timeOut, TimeUnit.SECONDS)

    clientBuilder.addInterceptor(HttpInterceptor(context))


        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        clientBuilder.addInterceptor(loggingInterceptor)


    return clientBuilder.build()
}

/**
 * Method used to create Retrofit instance
 * @param [okHttpClient] used to bind with retrofit
 * */
inline fun <reified T> createWebService(okHttpClient: OkHttpClient, serverUrl: String): T {
    val retrofit =
        Retrofit.Builder()
            .baseUrl(serverUrl)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create()).build()

    return retrofit.create(T::class.java)
}
