package com.example.bizita.service

import androidx.lifecycle.MutableLiveData
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

private const val NO_INTERNET_ERROR_CODE = 503

/**
 * Common network call class to handle all the network calls
 * */
open class NetworkCall<T> {
    lateinit var call: Call<T>

    /**
     * makeCall Function helps to make network call
     * */
    fun makeCall(call: Call<T>): MutableLiveData<Resource<T>> {
        this.call = call
        val callBackKt = CallBackKt<T>()
        callBackKt.result.value = Resource.loading(null)
        this.call.clone().enqueue(callBackKt)
        return callBackKt.result
    }

    /**
     * Common network callback method to handle the response
     * */
    class CallBackKt<T> : Callback<T> {
        var result: MutableLiveData<Resource<T>> = MutableLiveData()

        /**
         * This method handle network call failure
         * */
        override fun onFailure(call: Call<T>, t: Throwable) {
            if (t is NoNetworkException) {
                result.value = Resource.error(
                    ApiFailureException(
                        "No internet connection, Please check you mobile data or Wi-fi",
                        null,
                        NO_INTERNET_ERROR_CODE
                    )
                )
            } else {
                result.value = Resource.error(ApiFailureException(t.localizedMessage, t, null))
            }

            t.printStackTrace()
        }

        /**
         * This method handle network call success
         * */
        override fun onResponse(call: Call<T>, response: Response<T>) {
            result.value = if (response.isSuccessful) {
                Resource.success(response.body())
            } else {
                Resource.error(getError(response.code()))
            }
        }

        private fun getError(code: Int): ApiFailureException {
            return ApiFailureException("Something went wrong", null, code)
        }
    }

    /**
     * [retry] method is retry last call
     * */
    fun retry(): MutableLiveData<Resource<T>> = makeCall(call)

    /**
     * This method is cancel the call
     * */
    fun cancel() {
        if (::call.isInitialized) {
            call.cancel()
        }
    }
}