package com.example.bizita.service

import com.example.bizita.model.UserDataResponseModel
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface DataSource {

    @GET("/tracking/viewreport.php")
    fun getDataList(
    ): Call<UserDataResponseModel>

}