
package com.example.bizita.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.bizita.R
import com.example.bizita.databinding.ActivityHomeBinding

class HomeActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
    }
}