package com.example.bizita.view

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.IBinder
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.example.bizita.R
import com.example.bizita.di.APP_SCOPE_ID
import com.example.bizita.di.APP_SCOPE_QUALIIFIRE
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.*
import org.koin.core.KoinComponent
import org.koin.core.inject
import org.koin.core.scope.Scope
import kotlin.coroutines.CoroutineContext
import kotlin.coroutines.EmptyCoroutineContext

abstract class BaseFragment(
    private val parentContext: CoroutineContext = EmptyCoroutineContext
) :
    Fragment(), KoinComponent {

    protected lateinit var coroutineScope: CoroutineScope
        private set

    val viewModelScope: Scope? get() = _currentScope


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        coroutineScope =
            CoroutineScope(Dispatchers.Main + parentContext + SupervisorJob(parentContext[Job]))
    }


    override fun onDestroy() {
        coroutineScope.cancel()
        super.onDestroy()
    }

    private fun internalCreateScope(): Scope? {
        val created = getKoin().getScopeOrNull(APP_SCOPE_ID) == null
        val scope = getKoin()
            .getOrCreateScope(APP_SCOPE_ID, APP_SCOPE_QUALIIFIRE)

        if (created) {
            scope.declare(this)
        }

        return scope
    }

    private var _currentScope = internalCreateScope()

    /**
     * Destroy/close out a logged in scope. Should only be called when a user is logging out
     */
    fun destroyScope() {
        _currentScope.also { scope ->
            _currentScope = null
            scope?.close()
        }
    }

    /**
     * Start a new logged in scope.  If one is already active then it is returned without creation
     */
    fun startcope(): Scope? = internalCreateScope().apply {
        _currentScope = this
    }

    protected fun showErrorMessage(message: String) {
        Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show()
    }

    protected fun showError(message: String, view: View) {
        val snack = Snackbar.make(
            view,
            message,
            Snackbar.LENGTH_INDEFINITE
        )
        snack.setAction(resources.getString(R.string.ok)) {
            snack.dismiss()
        }
        snack.show()
    }



}