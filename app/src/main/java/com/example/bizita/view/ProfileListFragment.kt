package com.example.bizita.view

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.navigation.fragment.NavHostFragment
import com.example.bizita.R
import com.example.bizita.databinding.FragmentMapHomeBinding
import com.example.bizita.databinding.FragmentProfileListBinding
import com.example.bizita.listener.ApdapterListener
import com.example.bizita.viewmodel.AppViewModel
import org.koin.core.KoinComponent
import org.koin.android.viewmodel.scope.getViewModel


class ProfileListFragment : BaseFragment() {

    private lateinit var binding: FragmentProfileListBinding

    private val viewModel by lazy {
        viewModelScope?.getViewModel<AppViewModel>(
            requireParentFragment()
        )
    }
    private val adapter by lazy {
        ProfileListAdapter(this,requireContext(),object  : ApdapterListener{
            override fun onItemClick(position: Int, value: String) {
                NavHostFragment.findNavController(this@ProfileListFragment).navigate(R.id.action_profileListFragment_to_profileDetailFragment,
                Bundle().apply
                 { this.putString("value",value)
                 this.putInt("position",position)})
            }
        })
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            layoutInflater,
            R.layout.fragment_profile_list,
            container,
            false
        )
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        binding.rvUserList.adapter = adapter

        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.ivBack.setOnClickListener {
            requireActivity().onBackPressed()
        }
        viewModel?.dataList?.observe(viewLifecycleOwner, Observer {
            adapter.setDataList(it.success?: emptyList())
        })
    }

   }