package com.example.bizita.view

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.MainThread
import androidx.lifecycle.LifecycleOwner
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.bizita.databinding.ContactItemBinding
import com.example.bizita.listener.ApdapterListener
import com.example.bizita.model.AdapterBinder
import com.example.bizita.model.Succes

class ProfileListAdapter(private val lifecycleOwner: LifecycleOwner, private val context: Context, private val adapterListener : ApdapterListener) :
    RecyclerView.Adapter<ProfileListAdapter.ContactViewHolder>() {

    private var listItem: List<Succes> = emptyList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val itemBinding = ContactItemBinding.inflate(layoutInflater, parent, false)
        itemBinding.lifecycleOwner = lifecycleOwner
        return ContactViewHolder(
            itemBinding
        )
    }

    override fun getItemCount(): Int = listItem.size


    override fun onBindViewHolder(holder: ContactViewHolder, position: Int) {
        holder.bind()
    }


    fun setDataList(listItem: List<Succes>) {
        this.listItem = listItem
        notifyDataSetChanged()
    }


    inner class ContactViewHolder(
        private val itemBinding: ContactItemBinding
    ) : RecyclerView.ViewHolder(itemBinding.root) {
        private val binder = AdapterBinder()

        init {
            itemBinding.binder = binder
        }


        @MainThread
        fun bind() {
            binder.contactList(listItem[adapterPosition])
            itemBinding.executePendingBindings()
            Glide.with(context)
                .load(listItem[adapterPosition].image)
                .circleCrop()
                .into(itemBinding.ivImage)
            itemBinding.cvContact.setOnClickListener {
                adapterListener.onItemClick(adapterPosition,"")
            }
        }
    }
}
