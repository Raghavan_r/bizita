package com.example.bizita.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.navigation.fragment.NavHostFragment
import com.example.bizita.R
import com.example.bizita.databinding.FragmentMapHomeBinding
import com.example.bizita.model.Location
import com.example.bizita.viewmodel.AppViewModel
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.snackbar.Snackbar
import org.koin.android.viewmodel.scope.getViewModel


class MapHomeFragment : BaseFragment(), OnMapReadyCallback {
    private lateinit var binding: FragmentMapHomeBinding
    private var mMap: GoogleMap? = null

    private val viewModel by lazy {
        viewModelScope?.getViewModel<AppViewModel>(
            requireParentFragment()
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            layoutInflater,
            R.layout.fragment_map_home,
            container,
            false
        )

        binding.ivUserProfile.setOnClickListener{
            NavHostFragment.findNavController(this).navigate(R.id.action_mapHomeFragment_to_profileListFragment)
        }

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val mapFragment =
            childFragmentManager.findFragmentById(R.id.map_fragment) as SupportMapFragment?
        mapFragment?.getMapAsync(this)

        viewModel?.dataList?.observe(viewLifecycleOwner, Observer {
            setMapValues(it.location)
        })
        viewModel?.isLoading?.observe(viewLifecycleOwner, Observer {
            if(it){
                binding.progressBar?.visibility = View.VISIBLE
                binding.mapFragment.visibility = View.GONE
            }else{
                binding.progressBar?.visibility = View.GONE
                binding.mapFragment.visibility = View.VISIBLE
            }
        })
        viewModel?.apiError?.observe(viewLifecycleOwner, Observer {
            showSnackBar(it.message?:"")
        })
    }

    override fun onMapReady(googleMap: GoogleMap?) {
        googleMap?.let { map ->
            mMap = map
        }
    }

    fun showSnackBar(message: String) {

            Snackbar.make(
                binding.root,
                message,
                Snackbar.LENGTH_SHORT
            ).show()


    }

    private fun setMapValues(location: List<Location>?) {
        val markersArray: ArrayList<Location> = location as ArrayList<Location>

        for (i in 0 until markersArray.size) {
            createMarker(
                markersArray[i].lat,
                markersArray[i].longg
            )
        }

        val coordinate =
            LatLng(markersArray[0].lat.toDouble(), markersArray[0].longg.toDouble()) //Store these lat lng values somewhere. These should be constant.

        val target = CameraUpdateFactory.newLatLngZoom(
            coordinate, 1f
        )
        mMap?.animateCamera(target)

    }

    private fun createMarker(lat: String, longg: String): Marker? {
        return mMap?.addMarker(
            MarkerOptions()
                .position(LatLng(lat.toDouble(), longg.toDouble()))
                .anchor(0.5f, 0.5f)
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED))
        )
    }


}