package com.example.bizita.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import com.example.bizita.R
import com.example.bizita.databinding.FragmentProfileDetailBinding
import com.example.bizita.databinding.FragmentProfileListBinding
import com.example.bizita.viewmodel.AppViewModel
import kotlinx.android.synthetic.main.fragment_profile_detail.*
import org.koin.android.viewmodel.scope.getViewModel


class ProfileDetailFragment : BaseFragment() {

    private lateinit var binding: FragmentProfileDetailBinding

    private val viewModel by lazy {
        viewModelScope?.getViewModel<AppViewModel>(
            requireParentFragment()
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            layoutInflater,
            R.layout.fragment_profile_detail,
            container,
            false
        )

        binding.lifecycleOwner = this
        binding.viewModel = viewModel

        viewModel?.getSelectedValue(arguments?.getString("value"),arguments?.getInt("position"))

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel?.selectedProfile?.observe(viewLifecycleOwner, Observer {
            Glide.with(requireContext())
                .load(it.image)
                .circleCrop()
                .into(binding.ivProfile)
        })

        binding.ivBack.setOnClickListener {
            requireActivity().onBackPressed()
        }
    }


}