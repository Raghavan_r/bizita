package com.example.bizita.view

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.example.bizita.R
import kotlinx.coroutines.*

private const val SPLASH_DISPLAY_LENGTH: Long = 2000
class MainActivity : AppCompatActivity() {
    private val activityScope = CoroutineScope(Dispatchers.Main)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        activityScope.launch {
            delay(SPLASH_DISPLAY_LENGTH)
            startActivity(Intent(this@MainActivity,HomeActivity::class.java))
            finish()
        }
    }

    override fun onPause() {
        super.onPause()
        activityScope.cancel()
    }
}