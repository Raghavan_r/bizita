package com.example.bizita.di


import com.example.bizita.repo.AppRepo
import com.example.bizita.viewmodel.AppViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModules = module {

    scope(APP_SCOPE_QUALIIFIRE) {
        viewModel { AppViewModel(get()) }
    }
}


val repoModule = module {

    scope(APP_SCOPE_QUALIIFIRE) {
        scoped {
            AppRepo(get())
        }
    }
}
