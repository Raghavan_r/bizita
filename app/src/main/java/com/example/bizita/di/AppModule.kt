package com.example.bizita.di


import org.koin.android.ext.koin.androidContext
import org.koin.core.qualifier.named
import org.koin.dsl.module

/**
 * ScopeID for the current logged in scope
 */
const val APP_SCOPE_ID = "APP_SCOPE"

/**
 * Qualifier for the logged in scope
 */
val APP_SCOPE_QUALIIFIRE = named(APP_SCOPE_ID)

