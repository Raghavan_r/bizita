package com.example.bizita.application

import android.app.Application
import com.example.bizita.di.repoModule
import com.example.bizita.di.viewModelModules
import com.example.bizita.service.configModule
import com.example.bizita.service.remoteDataSourceModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.koin.core.logger.Level

class BizitaApplication: Application() {


    override fun onCreate() {
        super.onCreate()
        stopKoin()
        startKoin {
            androidLogger(Level.DEBUG)
            androidContext(this@BizitaApplication)
            modules(listOf(
                configModule,
                viewModelModules,
                remoteDataSourceModule, repoModule
            ))
        }
    }
}