package com.example.bizita.viewmodel

import androidx.lifecycle.*
import com.example.bizita.model.Succes
import com.example.bizita.model.UserDataResponseModel
import com.example.bizita.repo.AppRepo
import com.example.bizita.service.ApiFailureException
import com.example.bizita.service.Resource
import com.example.bizita.service.Status
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class AppViewModel(private val appRepo: AppRepo)  : ViewModel(){

    val selectedProfile = MutableLiveData<Succes>()

    val userName: LiveData<String> = MediatorLiveData<String>().apply {
        addSource(selectedProfile) {
            value = it.name
        }
    }
    val description: LiveData<String> = MediatorLiveData<String>().apply {
        addSource(selectedProfile) {
            value = it.description
        }
    }

    val address: LiveData<String> = MediatorLiveData<String>().apply {
        addSource(selectedProfile) {
            value = "Address: "+it.address
        }
    }
    val contact: LiveData<String> = MediatorLiveData<String>().apply {
        addSource(selectedProfile) {
            value = "Contact: "+it.contact
        }
    }
    val empCode: LiveData<String> = MediatorLiveData<String>().apply {
        addSource(selectedProfile) {
            value = "Employee Code: "+it.empcode
        }
    }

    val category: LiveData<String> = MediatorLiveData<String>().apply {
        addSource(selectedProfile) {
            value = "Category: "+it.category
        }
    }

    private val _apiError = MutableLiveData<ApiFailureException>()
    val apiError: LiveData<ApiFailureException> = _apiError

    private val _isLoading = MutableLiveData<Boolean>()
    val isLoading: LiveData<Boolean> = _isLoading

    private val _dataList = MutableLiveData<UserDataResponseModel>()
    val dataList: LiveData<UserDataResponseModel> =
        _dataList

    private val callObserver: Observer<Resource<UserDataResponseModel>> =
        Observer { t -> processResponse(t) }

    init {
        getDataList()
    }

    private fun getDataList() {
        viewModelScope.launch(Dispatchers.Main) {
            appRepo.getDataList().observeForever { callObserver.onChanged(it) }
        }
    }

    private fun processResponse(response: Resource<UserDataResponseModel>?) {
        when (response?.status) {
            Status.SUCCESS -> {
                _isLoading.value = false
                _dataList.value = response.data
            }
            Status.ERROR -> {
                _apiError.value = response.apiError
                _isLoading.value = false
            }
            Status.LOADING -> {
                _isLoading.value = true
            }
        }
    }

    fun getSelectedValue(value: String?, position : Int?) {
        if (position != null) {
            selectedProfile.value = _dataList.value?.success?.get(position)
        }
    }



}